package santoso.eriawan.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnPlay -> {
                audioPlay(posLaguSkrg)
            }
            R.id.btnNext -> {
                audioNext()
            }
            R.id.btnPrev -> {
                audioPrev()
            }
            R.id.btnStop -> {
                audioStop()
            }
        }
    }

//    val daftarLagu = intArrayOf(R.raw.musik_jiyuu, R.raw.musik_kana, R.raw.musik_resister)
//    val daftarVideo = intArrayOf(R.raw.vid_1, R.raw.vid_2, R.raw.vid_3)
//    val daftarCover = intArrayOf(R.drawable.jiyuu, R.drawable.kana, R.drawable.resister)
//    val daftarJudul = arrayOf("Music 1", "Music 2", "Music 3")

    var posLaguSkrg = 0

    //    var posVidSkrg = 0
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController
    lateinit var db: SQLiteDatabase
    lateinit var adapter : ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        lsMusik.setOnItemClickListener(itemClick)
//        mediaController.setPrevNextListeners(nextVid,prevVid)
//        mediaController.setAnchorView(videoView2)
//        videoView2.setMediaController(mediaController)
//        videoSet(posVidSkrg)
    }


    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        posLaguSkrg=position
        audioStop()
        audioPlay(position)
    }
    fun showDataMusik(){
        val cursor : Cursor = db.query("musik", arrayOf("id_musik as _id","id_cover","judul_musik"),null,null,null,null,"id_musik asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_musik,cursor,
            arrayOf("_id","id_cover","judul_musik"), intArrayOf(R.id.txidMusik, R.id.txidCover, R.id.txJudulMusik),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMusik.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataMusik()
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(pos : Int){
        val c: Cursor = lsMusik.adapter.getItem(pos) as Cursor
        var id_musik = c.getInt(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul_musik = c.getString(c.getColumnIndex("judul_musik"))

        mediaPlayer.stop()
        mediaPlayer = MediaPlayer.create(this, id_musik)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(id_cover)
        txJudulLagu.setText(judul_musik)
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread,50)
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(posLaguSkrg<(lsMusik.adapter.count-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = lsMusik.adapter.count-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this,50)
        }
    }

}