package santoso.eriawan.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "player"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMusik= "create table musik(id_musik text primary key, id_cover text not null, judul_musik text not null)"
        val insMusik= "insert into musik(id_musik,id_cover,judul_musik) values " +
                "('0x7f0c0000','0x7f060061','Link Horizon - Jiyuu no Tsubasa')," +
                "('0x7f0c0001','0x7f060062','Kanna-Boon - Silhoulete')," +
                "('0x7f0c0002','0x7f06006f','Asca - Resister')"
        db?.execSQL(tMusik)
        db?.execSQL(insMusik)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}